﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace automatyKom
{
    class Cell
    {
        public Rectangle body;
        static int idCount = 0;
        public int positionX;
        public int positionY;
        public int value;
        public int size;
        public bool changeStateFlag;
        public bool onBorder;
        public double ro;
        public double roMean;
        public bool toRecrystallization;
        public bool recrystallized;
        public Cell recristallizedNeighbour;

        public int id;
        public delegate void DelegeteCounter(int i, int j, int maxI, int minI, int maxJ, int minJ);
        public DelegeteCounter countNeighbours;
        public SolidColorBrush color;
        static Random rand = new Random();
        public Dictionary<int, int> neighbours;

        public Cell(Brush color, int size, int coordX, int coordY, int val)
        {
            this.recrystallized = false;
            this.toRecrystallization = false;
            this.body = new Rectangle();
            this.body.Width = size;
            this.body.Height = size;
           // SolidColorBrush blackBrush = new SolidColorBrush(Colors.Black);
           // this.body.Stroke = blackBrush;
            this.changeStateFlag = false;
            this.id = 0;
            this.positionX = coordX;
            this.positionY = coordY;
            this.body.Fill = color;
            this.value = val;
            this.size = size;
            this.neighbours = new Dictionary<int, int>();
            this.onBorder = false;
            this.ro = 0;
            this.roMean = 0;
            this.body.MouseLeftButtonDown += (source, e) =>
            {
                onMouseClick(false);
            };
        }

        public void onMouseClick(bool reclistalisation)
        {
            if (reclistalisation)
            {
                this.ro = 0;
                this.roMean = 0;
                this.recrystallized = true;
            }

            if (this.value == 0)
            {
                this.value = 1;
            }
            else if (this.value == 1)
            {
                setColor(Brushes.LightGray);
                this.value = 0;
            }

            if (this.value == 0)
            {
                Cell.idCount--;
                this.id = 0;
            }
            else
            {
                Cell.idCount++;
                this.id = Cell.idCount;
                // this.Content = this.id;
                int R = rand.Next(0, 255);
                int G = rand.Next(0, 255);
                int B = rand.Next(0, 255);
                this.color = new SolidColorBrush(Color.FromArgb(255, (Byte)(R), (Byte)(G), (Byte)(B)));
                setColor(this.color);
            }
        }

        public void recrystallize()
        {
            Cell.idCount++;
            this.id = Cell.idCount;
            int R = rand.Next(0, 255);
            int G = rand.Next(0, 255);
            int B = rand.Next(0, 255);
            this.color = new SolidColorBrush(Color.FromArgb(255, (Byte)(R), (Byte)(G), (Byte)(B)));
            setColor(this.color);
            this.toRecrystallization = false;
            this.ro = 0;
            this.roMean = 0;
            this.recrystallized = true;
        }

        public void setValue(int value)
        {
            this.value = value;
            if (value == 1)
            {
                setColor(this.color);

            }
            else if (value == 0)
            {
                setColor(Brushes.LightGray);
            }
            else
                setColor(Brushes.Red);
        }

        public void setColor(Brush color)
        {
            this.body.Fill = color;
        }

        public void setChangeStateFlag()
        {
            this.changeStateFlag = true;
        }

        public void changeState(int neighbourId, SolidColorBrush neighbourColor)
        {
            this.id = neighbourId;
            this.color = neighbourColor;
            this.setValue(1);
        }

        public void changeRecState(int neighbourId, SolidColorBrush neighbourColor)
        {
            this.id = neighbourId;
            this.color = neighbourColor;
            setColor(this.color);
            this.recrystallized = true;
            this.ro = 0;
            this.roMean = 0;
        }

        public void changeRecState()
        {
            this.toRecrystallization = true;
        }

        public void resetState()
        {
            this.changeStateFlag = false;
        }

        public void resetContent()
        {
            if (value == 1)
            {
                Cell.idCount--;
                this.id = 0;
                setColor(Brushes.LightGray);
            }
        }

    }
}
