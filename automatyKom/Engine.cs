﻿using automatyKom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using static automatyKom.Cell;

class Engine
{
    class Coord
    {
        public int X;
        public int Y;
        private bool _old;
        public bool old
        {
            get
            {
                return _old;
            }
            set
            {
                _old = value;
            }
        }

        public Coord(int x, int y)
        {
            this.X = x;
            this.Y = y;
            this.old = false;
        }

        public Coord()
        {
            this.old = false;
        }
    }

    public Cell[,] cells;

    public double A, B;
    public double roCritical;

    public Engine(bool cGameField, double a, double b, double roC)
    {
        this.circularGameField = cGameField;
        this.A = a;
        this.B = b;
        this.roCritical = roC;
    }

    public double countRo(double t)
    {

        return A / B + (1 - A / B) * Math.Exp(-B * t);
    }

    public void isOnBorder(int i, int j, int maxI, int minI, int maxJ, int minJ)
    {
        if (cells[minI, minJ].id != cells[i, j].id && cells[minI, minJ].id != 0) //1
        {
            cells[i, j].onBorder = true;
        }
        else if (cells[i, minJ].id != cells[i, j].id && cells[i, minJ].id != 0) //2
        {
            cells[i, j].onBorder = true;
        }
        else if (cells[maxI, minJ].id != cells[i, j].id && cells[maxI, minJ].id != 0) //3
        {
            cells[i, j].onBorder = true;
        }
        else if (cells[minI, j].id != cells[i, j].id && cells[minI, j].id != 0) //4
        {
            cells[i, j].onBorder = true;
        }
        else if (cells[maxI, j].id != cells[i, j].id && cells[maxI, j].id != 0) //5
        {
            cells[i, j].onBorder = true;
        }
        else if (cells[minI, maxJ].id != cells[i, j].id && cells[minI, maxJ].id != 0) //6
        {
            cells[i, j].onBorder = true;
        }
        else if (cells[i, maxJ].id != cells[i, j].id && cells[i, maxJ].id != 0) //7
        {
            cells[i, j].onBorder = true;
        }
        else if (cells[maxI, maxJ].id != cells[i, j].id && cells[maxI, maxJ].id != 0) //8
        {
            cells[i, j].onBorder = true;
        }
        else
            cells[i, j].onBorder = false;
    }

    public bool checkReclistallizedNeighbour(int i, int j, int maxI, int minI, int maxJ, int minJ)
    {
        if (cells[minI, minJ].recrystallized) //1
        {
            cells[i, j].recristallizedNeighbour = cells[minI, minJ];
            return true;
        }
        if (cells[i, minJ].recrystallized) //2
        {
            cells[i, j].recristallizedNeighbour = cells[i, minJ];
            return true;
        }
        if (cells[maxI, minJ].recrystallized) //3
        {
            cells[i, j].recristallizedNeighbour = cells[maxI, minJ];
            return true;
        }
        if (cells[minI, j].recrystallized) //4
        {
            cells[i, j].recristallizedNeighbour = cells[minI, j];
            return true;
        }
        if (cells[maxI, j].recrystallized) //5
        {
            cells[i, j].recristallizedNeighbour = cells[maxI, j];
            return true;
        }
        if (cells[minI, maxJ].recrystallized) //6
        {
            cells[i, j].recristallizedNeighbour = cells[minI, maxJ];
            return true;
        }
        if (cells[i, maxJ].recrystallized) //7
        {
            cells[i, j].recristallizedNeighbour = cells[i, maxJ];
            return true;
        }
        if (cells[maxI, maxJ].recrystallized) //8
        {
            cells[i, j].recristallizedNeighbour = cells[maxI, maxJ];
            return true;
        }

        return false;
    }

    static Random rand = new Random();
    bool circularGameField;

    public void countReclistallization(int N, double t,double timeStep)
    {
        for (int j = 0; j < N; j++)
        {
            for (int i = 0; i < N; i++)
            {
                cells[i, j].resetState();

                if (t != 0)
                    cells[i, j].roMean = countRo(t) / (N * N) - cells[i, j].ro;
                else
                    cells[i, j].roMean = countRo(t) / (N * N);

               // cells[i, j].roMean = cells[i, j].ro / (N * N);

                if (cells[i, j].onBorder)
                {
                    cells[i, j].ro = cells[i, j].roMean * (rand.Next(120, 180) * 0.01);
                }
                else
                {
                    cells[i, j].ro = cells[i, j].roMean * (rand.Next(0, 30) * 0.01);
                }

                if (cells[i, j].ro > countRo(0.065)/(N*N))
                {
                    cells[i, j].changeRecState();
                }

                if (i > 0 && i < N - 1 && j > 0 && j < N - 1)
                {
                    if (checkReclistallizedNeighbour(i, j, i + 1, i - 1, j + 1, j - 1))
                    {
                        cells[i, j].setChangeStateFlag();
                    }
                }
                else if (circularGameField)
                {
                    if (checkReclistallizedNeighbour(i, j, getMod(i + 1, N), getMod(i - 1, N), getMod(j + 1, N), getMod(j - 1, N)))
                    {
                        cells[i, j].setChangeStateFlag();
                    }
                }

            }
        }

        //aktualny krok
        foreach (Cell cell in cells)
        {
            if (cell.toRecrystallization && !cell.recrystallized)
            {
                cell.recrystallize();
            }

            if (cell.changeStateFlag && !cell.recrystallized)
            {
                cell.changeRecState(cell.recristallizedNeighbour.id, cell.recristallizedNeighbour.color);
            }
        }
    }

    public void getAllSeedBorders(int N)
    {
        for (int j = 0; j < N; j++)
        {
            for (int i = 0; i < N; i++)
            {
                if ((i > 0 && i < N - 1 && j > 0 && j < N - 1))
                    isOnBorder(i, j, i + 1, i - 1, j + 1, j - 1);
                else if (circularGameField)
                    isOnBorder(i, j, getMod(i + 1, N), getMod(i - 1, N), getMod(j + 1, N), getMod(j - 1, N));
            }
        }
    }




    public void firstCount(int N, int cellSize, int comboboxTypeIndex)
    {
        cells = new Cell[N, N];

        for (int j = 0; j < N; j++)
        {
            for (int i = 0; i < N; i++)
            {
                cells[i, j] = new Cell(Brushes.LightGray, cellSize, i, j, 0);
                setCunterFunction(comboboxTypeIndex, cells[i, j]);
            }
        }
    }

    public void uniformCellSelect(int xDiff, int yDiff)
    {
        foreach (Cell cell in cells)
        {
            if (cell.positionX % xDiff == 0 && cell.positionY % yDiff == 0)
            {
                cell.onMouseClick(false);
            }
        }
    }

    public void randomCellSelect(int N, int max)
    {
        for (int i = max; max > 0; max--)
        {
            cells[rand.Next(1, N - 1), rand.Next(1, N - 1)].onMouseClick(false);
        }
    }

    int getDistance(Coord p1, Coord p2)
    {
        return (int)Math.Sqrt(Math.Pow(p2.X - p1.X, 2) + Math.Pow(p2.Y - p1.Y, 2));
    }

    public void resetCells()
    {
        foreach (Cell cell in cells)
        {
            cell.resetContent();
        }
    }

    public void radCellSelect(int rad, int number, int N)
    {
        List<Coord> acltualAddedCellsPosition = new List<Coord>();

        foreach (Cell cell in cells)
        {
            if (cell.id != 0)
            {
                Coord tmp = new Coord(cell.positionX, cell.positionY);
                tmp.old = true;
                acltualAddedCellsPosition.Add(tmp);
            }

        }

        for (int i = 0; i < number; i++)
        {
            Coord newPosition = new Coord();
            newPosition.X = rand.Next(1, N - 1);
            newPosition.Y = rand.Next(1, N - 1);

            if (acltualAddedCellsPosition.Count > 0)
            {
                bool positionOk = true;
                foreach (Coord cellPosition in acltualAddedCellsPosition)
                {
                    if (getDistance(newPosition, cellPosition) < 2 * rad)
                    {
                        positionOk = false;
                    }
                }

                if (positionOk)
                    acltualAddedCellsPosition.Add(newPosition);
            }
            else
            {
                acltualAddedCellsPosition.Add(newPosition);
            }
        }

        foreach (Coord cellPosition in acltualAddedCellsPosition)
        {
            if (!cellPosition.old)
                cells[(int)cellPosition.X, (int)cellPosition.Y].onMouseClick(false);
        }
    }

    public void chceckConditions(int i, int j)
    {
        if (cells[i, j].neighbours.Count > 0)
        {
            cells[i, j].setChangeStateFlag();
        }
    }

    public void countNeighboursMoor(int i, int j, int maxI, int minI, int maxJ, int minJ)
    {
        if (cells[minI, minJ].id != 0) //1
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[minI, minJ].id))
            {
                cells[i, j].neighbours.Add(cells[minI, minJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[minI, minJ].id] = cells[i, j].neighbours[cells[minI, minJ].id]++;
            }
        }
        if (cells[i, minJ].id != 0) //2
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[i, minJ].id))
            {
                cells[i, j].neighbours.Add(cells[i, minJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[i, minJ].id] = cells[i, j].neighbours[cells[i, minJ].id]++;
            }
        }
        if (cells[maxI, minJ].id != 0) //3
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[maxI, minJ].id))
            {
                cells[i, j].neighbours.Add(cells[maxI, minJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[maxI, minJ].id] = cells[i, j].neighbours[cells[maxI, minJ].id]++;
            }
        }
        if (cells[minI, j].id != 0) //4
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[minI, j].id))
            {
                cells[i, j].neighbours.Add(cells[minI, j].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[minI, j].id] = cells[i, j].neighbours[cells[minI, j].id]++;
            }
        }
        if (cells[maxI, j].id != 0) //5
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[maxI, j].id))
            {
                cells[i, j].neighbours.Add(cells[maxI, j].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[maxI, j].id] = cells[i, j].neighbours[cells[maxI, j].id]++;
            }
        }
        if (cells[minI, maxJ].id != 0) //6
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[minI, maxJ].id))
            {
                cells[i, j].neighbours.Add(cells[minI, maxJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[minI, maxJ].id] = cells[i, j].neighbours[cells[minI, maxJ].id]++;
            }
        }
        if (cells[i, maxJ].id != 0) //7
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[i, maxJ].id))
            {
                cells[i, j].neighbours.Add(cells[i, maxJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[i, maxJ].id] = cells[i, j].neighbours[cells[i, maxJ].id]++;
            }
        }
        if (cells[maxI, maxJ].id != 0) //8
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[maxI, maxJ].id))
            {
                cells[i, j].neighbours.Add(cells[maxI, maxJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[maxI, maxJ].id] = cells[i, j].neighbours[cells[maxI, maxJ].id]++;
            }
        }
    }

    public void countNeighboursNeuman(int i, int j, int maxI, int minI, int maxJ, int minJ)
    {
        //  minI,minJ   i,minJ  maxI,minJ
        //  minI,j      i,j     maxI,j
        //  minI,maxJ   i,maxJ  maxI,maxJ

        if (cells[i, minJ].id != 0) //2
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[i, minJ].id))
            {
                cells[i, j].neighbours.Add(cells[i, minJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[i, minJ].id] = cells[i, j].neighbours[cells[i, minJ].id]++;
            }
        }
        if (cells[minI, j].id != 0) //4
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[minI, j].id))
            {
                cells[i, j].neighbours.Add(cells[minI, j].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[minI, j].id] = cells[i, j].neighbours[cells[minI, j].id]++;
            }
        }
        if (cells[maxI, j].id != 0) //5
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[maxI, j].id))
            {
                cells[i, j].neighbours.Add(cells[maxI, j].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[maxI, j].id] = cells[i, j].neighbours[cells[maxI, j].id]++;
            }
        }
        if (cells[i, maxJ].id != 0) //7
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[i, maxJ].id))
            {
                cells[i, j].neighbours.Add(cells[i, maxJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[i, maxJ].id] = cells[i, j].neighbours[cells[i, maxJ].id]++;
            }
        }
    }

    public void countNeighboursHexLeft(int i, int j, int maxI, int minI, int maxJ, int minJ)
    {
        if (cells[minI, minJ].id != 0) //1
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[minI, minJ].id))
            {
                cells[i, j].neighbours.Add(cells[minI, minJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[minI, minJ].id] = cells[i, j].neighbours[cells[minI, minJ].id]++;
            }
        }
        if (cells[i, minJ].id != 0) //2
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[i, minJ].id))
            {
                cells[i, j].neighbours.Add(cells[i, minJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[i, minJ].id] = cells[i, j].neighbours[cells[i, minJ].id]++;
            }
        }
        if (cells[minI, j].id != 0) //4
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[minI, j].id))
            {
                cells[i, j].neighbours.Add(cells[minI, j].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[minI, j].id] = cells[i, j].neighbours[cells[minI, j].id]++;
            }
        }
        if (cells[maxI, j].id != 0) //5
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[maxI, j].id))
            {
                cells[i, j].neighbours.Add(cells[maxI, j].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[maxI, j].id] = cells[i, j].neighbours[cells[maxI, j].id]++;
            }
        }
        if (cells[i, maxJ].id != 0) //7
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[i, maxJ].id))
            {
                cells[i, j].neighbours.Add(cells[i, maxJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[i, maxJ].id] = cells[i, j].neighbours[cells[i, maxJ].id]++;
            }
        }
        if (cells[maxI, maxJ].id != 0) //8
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[maxI, maxJ].id))
            {
                cells[i, j].neighbours.Add(cells[maxI, maxJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[maxI, maxJ].id] = cells[i, j].neighbours[cells[maxI, maxJ].id]++;
            }
        }
    }

    public void countNeighboursHexRight(int i, int j, int maxI, int minI, int maxJ, int minJ)
    {

        if (cells[i, minJ].id != 0) //2
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[i, minJ].id))
            {
                cells[i, j].neighbours.Add(cells[i, minJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[i, minJ].id] = cells[i, j].neighbours[cells[i, minJ].id]++;
            }
        }
        if (cells[maxI, minJ].id != 0) //3
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[maxI, minJ].id))
            {
                cells[i, j].neighbours.Add(cells[maxI, minJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[maxI, minJ].id] = cells[i, j].neighbours[cells[maxI, minJ].id]++;
            }
        }
        if (cells[minI, j].id != 0) //4
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[minI, j].id))
            {
                cells[i, j].neighbours.Add(cells[minI, j].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[minI, j].id] = cells[i, j].neighbours[cells[minI, j].id]++;
            }
        }
        if (cells[maxI, j].id != 0) //5
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[maxI, j].id))
            {
                cells[i, j].neighbours.Add(cells[maxI, j].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[maxI, j].id] = cells[i, j].neighbours[cells[maxI, j].id]++;
            }
        }
        if (cells[minI, maxJ].id != 0) //6
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[minI, maxJ].id))
            {
                cells[i, j].neighbours.Add(cells[minI, maxJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[minI, maxJ].id] = cells[i, j].neighbours[cells[minI, maxJ].id]++;
            }
        }
        if (cells[i, maxJ].id != 0) //7
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[i, maxJ].id))
            {
                cells[i, j].neighbours.Add(cells[i, maxJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[i, maxJ].id] = cells[i, j].neighbours[cells[i, maxJ].id]++;
            }
        }
    }

    public void countNeighboursPentTop(int i, int j, int maxI, int minI, int maxJ, int minJ)
    {
        if (cells[minI, minJ].id != 0) //1
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[minI, minJ].id))
            {
                cells[i, j].neighbours.Add(cells[minI, minJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[minI, minJ].id] = cells[i, j].neighbours[cells[minI, minJ].id]++;
            }
        }
        if (cells[i, minJ].id != 0) //2
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[i, minJ].id))
            {
                cells[i, j].neighbours.Add(cells[i, minJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[i, minJ].id] = cells[i, j].neighbours[cells[i, minJ].id]++;
            }
        }
        if (cells[maxI, minJ].id != 0) //3
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[maxI, minJ].id))
            {
                cells[i, j].neighbours.Add(cells[maxI, minJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[maxI, minJ].id] = cells[i, j].neighbours[cells[maxI, minJ].id]++;
            }
        }
        if (cells[minI, j].id != 0) //4
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[minI, j].id))
            {
                cells[i, j].neighbours.Add(cells[minI, j].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[minI, j].id] = cells[i, j].neighbours[cells[minI, j].id]++;
            }
        }
        if (cells[maxI, j].id != 0) //5
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[maxI, j].id))
            {
                cells[i, j].neighbours.Add(cells[maxI, j].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[maxI, j].id] = cells[i, j].neighbours[cells[maxI, j].id]++;
            }
        }
    }

    public void countNeighboursPentBottom(int i, int j, int maxI, int minI, int maxJ, int minJ)
    {
        if (cells[minI, j].id != 0) //4
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[minI, j].id))
            {
                cells[i, j].neighbours.Add(cells[minI, j].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[minI, j].id] = cells[i, j].neighbours[cells[minI, j].id]++;
            }
        }
        if (cells[maxI, j].id != 0) //5
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[maxI, j].id))
            {
                cells[i, j].neighbours.Add(cells[maxI, j].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[maxI, j].id] = cells[i, j].neighbours[cells[maxI, j].id]++;
            }
        }
        if (cells[minI, maxJ].id != 0) //6
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[minI, maxJ].id))
            {
                cells[i, j].neighbours.Add(cells[minI, maxJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[minI, maxJ].id] = cells[i, j].neighbours[cells[minI, maxJ].id]++;
            }
        }
        if (cells[i, maxJ].id != 0) //7
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[i, maxJ].id))
            {
                cells[i, j].neighbours.Add(cells[i, maxJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[i, maxJ].id] = cells[i, j].neighbours[cells[i, maxJ].id]++;
            }
        }
        if (cells[maxI, maxJ].id != 0) //8
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[maxI, maxJ].id))
            {
                cells[i, j].neighbours.Add(cells[maxI, maxJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[maxI, maxJ].id] = cells[i, j].neighbours[cells[maxI, maxJ].id]++;
            }
        }
    }

    public void countNeighboursPentLeft(int i, int j, int maxI, int minI, int maxJ, int minJ)
    {
        if (cells[minI, minJ].id != 0) //1
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[minI, minJ].id))
            {
                cells[i, j].neighbours.Add(cells[minI, minJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[minI, minJ].id] = cells[i, j].neighbours[cells[minI, minJ].id]++;
            }
        }
        if (cells[i, minJ].id != 0) //2
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[i, minJ].id))
            {
                cells[i, j].neighbours.Add(cells[i, minJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[i, minJ].id] = cells[i, j].neighbours[cells[i, minJ].id]++;
            }
        }
        if (cells[minI, j].id != 0) //4
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[minI, j].id))
            {
                cells[i, j].neighbours.Add(cells[minI, j].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[minI, j].id] = cells[i, j].neighbours[cells[minI, j].id]++;
            }
        }
        if (cells[minI, maxJ].id != 0) //6
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[minI, maxJ].id))
            {
                cells[i, j].neighbours.Add(cells[minI, maxJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[minI, maxJ].id] = cells[i, j].neighbours[cells[minI, maxJ].id]++;
            }
        }
        if (cells[i, maxJ].id != 0) //7
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[i, maxJ].id))
            {
                cells[i, j].neighbours.Add(cells[i, maxJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[i, maxJ].id] = cells[i, j].neighbours[cells[i, maxJ].id]++;
            }
        }
    }

    public void countNeighboursPentRight(int i, int j, int maxI, int minI, int maxJ, int minJ)
    {
        if (cells[i, minJ].id != 0) //2
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[i, minJ].id))
            {
                cells[i, j].neighbours.Add(cells[i, minJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[i, minJ].id] = cells[i, j].neighbours[cells[i, minJ].id]++;
            }
        }
        if (cells[maxI, minJ].id != 0) //3
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[maxI, minJ].id))
            {
                cells[i, j].neighbours.Add(cells[maxI, minJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[maxI, minJ].id] = cells[i, j].neighbours[cells[maxI, minJ].id]++;
            }
        }
        if (cells[maxI, j].id != 0) //5
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[maxI, j].id))
            {
                cells[i, j].neighbours.Add(cells[maxI, j].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[maxI, j].id] = cells[i, j].neighbours[cells[maxI, j].id]++;
            }
        }
        if (cells[i, maxJ].id != 0) //7
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[i, maxJ].id))
            {
                cells[i, j].neighbours.Add(cells[i, maxJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[i, maxJ].id] = cells[i, j].neighbours[cells[i, maxJ].id]++;
            }
        }
        if (cells[maxI, maxJ].id != 0) //8
        {
            if (!cells[i, j].neighbours.ContainsKey(cells[maxI, maxJ].id))
            {
                cells[i, j].neighbours.Add(cells[maxI, maxJ].id, 1);
            }
            else
            {
                cells[i, j].neighbours[cells[maxI, maxJ].id] = cells[i, j].neighbours[cells[maxI, maxJ].id]++;
            }
        }
    }

    public void setCunterFunction(int index, Cell cell)
    {
        int randBool = rand.Next(0, 100);
        switch (index)
        {
            case 0:
                cell.countNeighbours = countNeighboursNeuman;
                break;
            case 1:
                cell.countNeighbours = countNeighboursMoor;
                break;
            case 2:
                cell.countNeighbours = countNeighboursHexLeft;
                break;
            case 3:
                cell.countNeighbours = countNeighboursHexRight;
                break;
            case 4:
                if (randBool <= 50)
                    cell.countNeighbours = countNeighboursHexRight;
                else
                    cell.countNeighbours = countNeighboursHexLeft;
                break;
            case 5:
                if (randBool <= 25)
                    cell.countNeighbours = countNeighboursPentRight;
                else if (randBool > 25 && randBool <= 50)
                    cell.countNeighbours = countNeighboursPentLeft;
                else if (randBool > 50 && randBool <= 75)
                    cell.countNeighbours = countNeighboursPentBottom;
                else
                    cell.countNeighbours = countNeighboursPentTop;
                break;
            case 6:
                if (randBool <= 12)
                    cell.countNeighbours = countNeighboursNeuman;
                else if (randBool > 12 && randBool <= 24)
                    cell.countNeighbours = countNeighboursMoor;
                else if (randBool > 24 && randBool <= 36)
                    cell.countNeighbours = countNeighboursHexLeft;
                else if (randBool > 36 && randBool <= 48)
                    cell.countNeighbours = countNeighboursHexRight;
                else if (randBool > 48 && randBool <= 60)
                    cell.countNeighbours = countNeighboursPentBottom;
                else if (randBool > 60 && randBool <= 72)
                    cell.countNeighbours = countNeighboursPentTop;
                else if (randBool > 72 && randBool <= 84)
                    cell.countNeighbours = countNeighboursPentLeft;
                else
                    cell.countNeighbours = countNeighboursPentRight;
                break;

            default:
                cell.countNeighbours = countNeighboursNeuman;
                break;
        }
    }


    int getMod(int x, int N)
    {
        int result = (Math.Abs(x * N) + x) % N; ;
        // Console.WriteLine(result);
        return result;
    }

    //  minI,minJ   i,minJ  maxI,minJ
    //  minI,j      i,j     maxI,j
    //  minI,maxJ   i,maxJ  maxI,maxJ

    public void count(int N)
    {
        for (int j = 0; j < N; j++)
        {
            for (int i = 0; i < N; i++)
            {
                //resetowanie stanu komórki
                cells[i, j].resetState();

                //jeżeli mają wszytkich sąsiadów ośmiu / komorki w środku - warunki nieperiodyczne
                if (i > 0 && i < N - 1 && j > 0 && j < N - 1)
                {

                    if (cells[i, j].id == 0) //komorki puste
                    {
                        //zliczanie sąsiadów
                        cells[i, j].countNeighbours(i, j, i + 1, i - 1, j + 1, j - 1);

                        //warunki
                        chceckConditions(i, j);
                    }
                }
                else if (circularGameField)  //warunki periodyczne
                {
                    if (cells[i, j].id == 0) //komorki puste
                    {
                        cells[i, j].countNeighbours(i, j, getMod(i + 1, N), getMod(i - 1, N), getMod(j + 1, N), getMod(j - 1, N));
                        chceckConditions(i, j);
                    }
                }
                else
                {
                    cells[i, j].setValue(-1);
                }
            }
        }

        foreach (Cell cell in cells)
        {
            if (cell.changeStateFlag)
            {
                var max = cell.neighbours.Aggregate((l, r) => l.Value > r.Value ? l : r);
                SolidColorBrush color = Brushes.LightGray;
                foreach (Cell c in cells)
                {
                    if (c.id == max.Key)
                    {
                        color = c.color;
                        break;
                    }
                }
                cell.changeState(max.Key, color);
            }
        }
    }
}

