﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace automatyKom
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool mainThreadStopped;
        int iteration;
        double fullTime;
        double timeStep;
        int N;
        Thread mainThread;
        Thread recThread;
        Engine engine;
        bool stopFlag;
        bool stopRecFlag;
        double speed;
        int cellSize;
        int threadCunter;
        double currentTime;
        double A,B;

        public MainWindow()
        {
            mainThreadStopped = false;
            InitializeComponent();
            iteration = Int32.Parse(textboxIter.Text);
            stopFlag = false;
            stopRecFlag = false;
            speed = Double.Parse(textboxSpeed.Text);
            cellSize = Int32.Parse(comboBoxSize.Text);
            mainThread = new Thread(threadMethod);
            recThread = new Thread(threadRecMethod);
            N = (int)canvas.Width / cellSize;
            if (labelMeshSize != null)
                labelMeshSize.Content = N.ToString() + "x" + N.ToString();
            bool cGameField = dbCircularGameField.IsChecked == true ? true : false;

            A = 86710969050178.5;
            B = 9.41268203527779;
            currentTime = 0.0;
            if (tbTimeStep != null)
                timeStep = Double.Parse(tbTimeStep.Text);
            if (tbFullTime != null)
                fullTime = Double.Parse(tbFullTime.Text);
            engine = new Engine(cGameField, A, B, 4215840142323.42/ (N*N));
            engine.firstCount(N, cellSize, comboBoxType.SelectedIndex);
            drawMesh();
            labelRad.Visibility = Visibility.Hidden;
            sliderRad.Visibility = Visibility.Hidden;
            labelX.Visibility = Visibility.Hidden;
            labelY.Visibility = Visibility.Hidden;
            sliderX.Visibility = Visibility.Hidden;
            sliderY.Visibility = Visibility.Hidden;
            labelRand.Visibility = Visibility.Hidden;
            sliderRand.Visibility = Visibility.Hidden;
            threadCunter = 0;
            Closing += this.OnWindowClosing;
        }

        private void resetEngine()
        {
            A = 86710969050178.5;
            B = 9.41268203527779;
            currentTime = 0.0;
           

            iteration = Int32.Parse(textboxIter.Text);
            cellSize = Int32.Parse(comboBoxSize.Text);
            if(tbTimeStep!=null)
                timeStep = Double.Parse(tbTimeStep.Text);
            if (tbFullTime != null)
                fullTime = Double.Parse(tbFullTime.Text);
            speed = Double.Parse(textboxSpeed.Text);
            N = (int)canvas.Width / cellSize;
            if (labelMeshSize != null)
                labelMeshSize.Content = N.ToString() + "x" + N.ToString();
            bool cGameField = dbCircularGameField.IsChecked == true ? true : false;
            engine = new Engine(cGameField,A,B, 4215840142323.42 / (N * N));
            //stopFlag = true;
            iterationCountLabel.Content = "0";
            threadCunter = 0;
            // mainThread = new Thread(threadMethod);
            engine.firstCount(N, cellSize, comboBoxType.SelectedIndex);
            drawMesh();
        }

        private void drawMesh()
        {
            canvas.Children.Clear();
            for (int j = 0; j < N; j++)
            {
                for (int i = 0; i < N; i++)
                {
                    Canvas.SetLeft(engine.cells[i, j].body, engine.cells[i, j].size * engine.cells[i, j].positionX);
                    Canvas.SetTop(engine.cells[i, j].body, engine.cells[i, j].size * engine.cells[i, j].positionY);
                    canvas.Children.Add(engine.cells[i, j].body);
                }
            }
        }

        private void redrawMesh(int N)
        {
            engine.count(N);
        }

        private void redrawMeshRec(int N, double time, double timeStep)
        {
            engine.countReclistallization(N, time, timeStep);
        }

        private void startBtn_Click(object sender, RoutedEventArgs e)
        {
            //stopFlag = false;
            iteration = Int32.Parse(textboxIter.Text);
            if (mainThread.ThreadState == System.Threading.ThreadState.Unstarted)
                mainThread.Start();
            else if (mainThread.ThreadState == System.Threading.ThreadState.Suspended)
                mainThread.Resume();

        }

        private void resetBtn_Click(object sender, RoutedEventArgs e)
        {
            resetEngine();
        }

        private void stopBtn_Click(object sender, RoutedEventArgs e)
        {
            //stopFlag = true;
            if (mainThread.ThreadState != System.Threading.ThreadState.Unstarted && !mainThreadStopped)
                mainThread.Suspend();
        }

        private void threadMethod()
        {
            while (threadCunter <= iteration && !stopFlag)
            {
                Thread.Sleep(TimeSpan.FromSeconds(speed));
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                (ThreadStart)delegate ()
                {
                    redrawMesh(N);
                    iterationCountLabel.Content = threadCunter.ToString();
                    threadCunter++;
                    //Console.WriteLine("Working iter: " + threadCunter);
                });
            }
            Console.WriteLine("Thread terminating gracefully.");
            mainThreadStopped = true;
        }

        private void threadRecMethod()
        {
            while (currentTime <= fullTime && !stopRecFlag)
            {
                Thread.Sleep(TimeSpan.FromSeconds(1));
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                (ThreadStart)delegate ()
                {
                    redrawMeshRec(N, currentTime, timeStep);
                    iterationCountLabel.Content = currentTime.ToString();
                    currentTime+= timeStep;
                    //Console.WriteLine("Working iter: " + threadCunter);
                });
            }
            Console.WriteLine("Thread terminating gracefully.");
        }

        private void comboBoxZ_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (labelRad != null && sliderRad != null)
            {
                switch (comboBoxZ.SelectedIndex)
                {
                    case 0:
                        if (labelBottomSlider != null && labelTopSlider != null)
                        {
                            labelBottomSlider.Content = "";
                            labelTopSlider.Content = "";
                        }
                        labelRad.Visibility = Visibility.Hidden;
                        sliderRad.Visibility = Visibility.Hidden;
                        labelX.Visibility = Visibility.Hidden;
                        labelY.Visibility = Visibility.Hidden;
                        sliderX.Visibility = Visibility.Hidden;
                        sliderY.Visibility = Visibility.Hidden;
                        labelRand.Visibility = Visibility.Hidden;
                        sliderRand.Visibility = Visibility.Hidden;
                        break;
                    case 1:
                        if (labelBottomSlider != null && labelTopSlider != null)
                        {
                            labelBottomSlider.Content = sliderY.Value.ToString();
                            labelTopSlider.Content = sliderX.Value.ToString();
                        }
                        labelRad.Visibility = Visibility.Hidden;
                        sliderRad.Visibility = Visibility.Hidden;
                        labelX.Visibility = Visibility.Visible;
                        labelY.Visibility = Visibility.Visible;
                        sliderX.Visibility = Visibility.Visible;
                        sliderY.Visibility = Visibility.Visible;
                        labelRand.Visibility = Visibility.Hidden;
                        sliderRand.Visibility = Visibility.Hidden;
                        break;
                    case 2:
                        if (labelBottomSlider != null && labelTopSlider != null)
                        {
                            labelBottomSlider.Content = "";
                            labelTopSlider.Content = sliderRand.Value.ToString();
                        }
                        labelRad.Visibility = Visibility.Hidden;
                        sliderRad.Visibility = Visibility.Hidden;
                        labelX.Visibility = Visibility.Hidden;
                        labelY.Visibility = Visibility.Hidden;
                        sliderX.Visibility = Visibility.Hidden;
                        sliderY.Visibility = Visibility.Hidden;
                        labelRand.Visibility = Visibility.Visible;
                        sliderRand.Visibility = Visibility.Visible;
                        break;
                    case 3:
                        if (labelBottomSlider != null && labelTopSlider != null)
                        {
                            labelBottomSlider.Content = sliderRad.Value.ToString();
                            labelTopSlider.Content = sliderRand.Value.ToString();
                        }
                        labelRad.Visibility = Visibility.Visible;
                        sliderRad.Visibility = Visibility.Visible;
                        labelX.Visibility = Visibility.Hidden;
                        labelY.Visibility = Visibility.Hidden;
                        sliderX.Visibility = Visibility.Hidden;
                        sliderY.Visibility = Visibility.Hidden;
                        labelRand.Visibility = Visibility.Visible;
                        sliderRand.Visibility = Visibility.Visible;
                        break;
                    default:
                        labelRad.Visibility = Visibility.Hidden;
                        sliderRad.Visibility = Visibility.Hidden;
                        labelX.Visibility = Visibility.Hidden;
                        labelY.Visibility = Visibility.Hidden;
                        sliderX.Visibility = Visibility.Hidden;
                        sliderY.Visibility = Visibility.Hidden;
                        labelRand.Visibility = Visibility.Hidden;
                        sliderRand.Visibility = Visibility.Hidden;
                        break;
                }
            }
        }

        private void comboBoxType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            resetEngine();
        }

        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            switch (comboBoxZ.SelectedIndex)
            {
                case 0:

                    break;
                case 1:
                    engine.uniformCellSelect((int)sliderX.Value, (int)sliderY.Value);
                    break;
                case 2:
                    engine.randomCellSelect(this.N, (int)sliderRand.Value);
                    break;
                case 3:
                    engine.radCellSelect((int)sliderRad.Value, (int)sliderRand.Value, this.N);
                    break;
            }
        }

        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            if (mainThread.ThreadState == System.Threading.ThreadState.Suspended)
                mainThread.Resume();
            if (recThread.ThreadState == System.Threading.ThreadState.Suspended)
                recThread.Resume();
            stopFlag = true;
            stopRecFlag = true;
            Console.WriteLine("Program terminated");
        }

        private void sliderRad_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (sliderRad.Visibility == Visibility.Visible && labelBottomSlider != null)
                labelBottomSlider.Content = sliderRad.Value.ToString();
        }

        private void sliderRand_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (sliderRand.Visibility == Visibility.Visible && labelTopSlider != null)
                labelTopSlider.Content = sliderRand.Value.ToString();
        }

        private void sliderY_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (sliderY.Visibility == Visibility.Visible && labelBottomSlider != null)
                labelBottomSlider.Content = sliderY.Value.ToString();
        }

        private void btnRecStart_Click(object sender, RoutedEventArgs e)
        {
            fullTime = Double.Parse(tbFullTime.Text);
            timeStep = Double.Parse(tbTimeStep.Text);
            if (mainThread.ThreadState != System.Threading.ThreadState.Unstarted && !mainThreadStopped)
                mainThread.Suspend();
            engine.getAllSeedBorders(N);
            if (recThread.ThreadState == System.Threading.ThreadState.Unstarted)
                recThread.Start();
            else if (recThread.ThreadState == System.Threading.ThreadState.Suspended)
                recThread.Resume();
        }

        private void btnRecStop_Click(object sender, RoutedEventArgs e)
        {
            if (recThread.ThreadState != System.Threading.ThreadState.Unstarted)
                recThread.Suspend();
        }

        private void tbTimeStep_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void sliderX_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (sliderX.Visibility == Visibility.Visible && labelTopSlider != null)
                labelTopSlider.Content = sliderX.Value.ToString();
        }
    }
}
